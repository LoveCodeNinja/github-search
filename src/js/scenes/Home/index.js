import React, {
  Component
} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import axios from 'axios';
import { Input, Container } from 'reactstrap';

import Api from '../../apis/rest';
import { setResult } from '../../actions/global';
import ResultCard from '../../components/ResultCard';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: ''
    };
    this.timer = '';
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  async handleInputChange(e) {
    const { setResult } = this.props;
    const { value } = e.target;

    if(this.timer)
      clearTimeout(this.timer);
      if(value.length > 2) {
        this.timer = setTimeout(async () => {
          await axios.get(`https://api.github.com/search/repositories?q=${value}`)
          .then((res) => {
            const { status, data: {items} } = res;
            if(status === 200) {
              setResult(items);
            }
          });
        }, 300);
      }
  }

  render() {
    const { projects } = this.props;

    return (
      <Container className="home">
        <div className="action">
          <Input
            type="text"
            name="search"
            onChange={this.handleInputChange}
          />
        </div>
        <div className="project-list">
          {
            projects.map(item => (
              <ResultCard key={item.id} data={item} />
            ))
          }
        </div>
      </Container>
    );
  }
}

const mapStateToProps = (state) => ({
  projects: state.common.result
});

const mapDispatchToProps = dispatch => ({
  setResult: bindActionCreators(setResult, dispatch)
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Home));

