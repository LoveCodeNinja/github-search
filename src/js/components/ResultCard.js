import React, { Component } from 'react';

class ResultCard extends Component {
    render() {
        const { data: { svn_url, watchers_count, stargazers_count, full_name} } = this.props;
        
        return (
            <div className="result-card">
                <a href={svn_url} target="_blank">{full_name}</a>
                <div className="watchers-count">
                    { watchers_count }
                </div>
                <div className="stargazers-count">
                    { stargazers_count }
                </div>
            </div>
        );
    }
}

export default ResultCard;