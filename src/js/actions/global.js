import Types from './types';

export const setResult = data => (
  (dispatch) => {
    return dispatch({
      type: Types.SET_RESULT,
      data
    });
  }
);

