import Types from '../actions/types';

export const initialState = {
  result: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case Types.SET_RESULT:
      return {
        ...state,
        result: action.data
      };
    
    default:
      return state;
  }
};
